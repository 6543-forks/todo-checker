#!/bin/bash

ROOTDIR="$(dirname "$0")"
ROOTDIR="$(readlink -f "${ROOTDIR}")"
cd "${ROOTDIR}" || echo exit 3

FAILED="false"

function should_fail() {
  if [ "$1" == "255" ]; then
    echo PASS
  else
    echo FAILED
    [ "${FAILED}" == "false" ] && FAILED="true" || true
  fi
}

function should_pass() {
  if [ "$1" == "0" ]; then
    echo PASS
  else
    echo FAILED
    [ "${FAILED}" == "false" ] && FAILED="true" || true
  fi
}

echo -n "TEST: valid open TODO: "
{
  export CI_FORGE_URL=https://codeberg.org
  export CI_REPO_URL=https://codeberg.org/6543-forks/CI-TEST_foo
  export CI_REPO=6543-forks/CI-TEST_foo
  export CI_FORGE_TYPE=gitea

  cd "${ROOTDIR}"/tests/CI-TEST_foo/ || exit 3
  "${ROOTDIR}"/entry.sh
  should_pass "$?"
}

echo -n "TEST: fail on non open valid TODOs: "
{
  export CI_FORGE_URL=https://codeberg.org
  export CI_REPO_URL=https://codeberg.org/6543-forks/CI-TEST_foo
  export CI_REPO=6543-forks/CI-TEST_foo
  export CI_FORGE_TYPE=gitea

  cd "${ROOTDIR}"/tests/fail_issue_not_found/ || exit 3
  "${ROOTDIR}"/entry.sh > /dev/null
  should_fail "$?"
}

echo -n "TEST: fail on TODOs that are not valid: "
{
  export CI_FORGE_URL=https://codeberg.org
  export CI_REPO_URL=https://codeberg.org/6543-forks/CI-TEST_foo
  export CI_REPO=6543-forks/CI-TEST_foo
  export CI_FORGE_TYPE=gitea

  cd "${ROOTDIR}"/tests/errors/ || exit 3
  "${ROOTDIR}"/entry.sh > /dev/null
  should_fail "$?"
}

echo -n "TEST: ignore specific files: "
{
  export CI_FORGE_URL=https://codeberg.org
  export CI_REPO_URL=https://codeberg.org/6543-forks/CI-TEST_foo
  export CI_REPO=6543-forks/CI-TEST_foo
  export CI_FORGE_TYPE=gitea

  cd "${ROOTDIR}"/tests/ignore-files/ || exit 3
  "${ROOTDIR}"/entry.sh
  should_pass "$?"
}

[ "${FAILED}" == "true" ] && exit 1 || exit 0
