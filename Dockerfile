FROM alpine:edge

RUN apk add --no-cache ripgrep curl bash jq

ADD entry.sh /bin/entry.sh
ENTRYPOINT ["/bin/entry.sh"]
