#!/bin/bash
set -e

# first test if we have all variables we need
[ -z "${CI_REPO}" ] && { echo "'CI_REPO' not set"; exit 1; } || true
[ -z "${CI_REPO_URL}" ] && [ -z "${CI_FORGE_URL}" ] && { echo "'CI_REPO_URL' or 'CI_FORGE_URL' not set"; exit 1; } || true
[ -z "${CI_FORGE_TYPE}" ] && { echo "'CI_FORGE_TYPE' not set"; exit 1; } || true

# check currently unsupported forges
[ "${CI_FORGE_TYPE}" != "gitea" ] && { echo "${CI_FORGE_TYPE} is currently not supported"; exit 1; } || true

# set defaults and sanitize
PLUGIN_DEBUG="$(echo "${PLUGIN_DEBUG}" | tr '[:upper:]' '[:lower:]')"
[ -z "${PLUGIN_PREFIX_REGEX}" ] && { PLUGIN_PREFIX_REGEX='(TODO|FIXME)'; } || true
[ -z "${PLUGIN_MAIN_REGEX}" ] && { PLUGIN_MAIN_REGEX='( |)(\(|\(#)(?P<ISSUE_NUMBER>\d+)(\))'; } || true
WORKDIR="."
ARGS=()

function extract_forge_url() {
    URL="${CI_FORGE_URL}"
    # if CI_FORGE_URL is not set calculate based on repo link and repo fullname
    [ -z "${CI_FORGE_URL}" ] &&  URL="${CI_REPO_URL%"${CI_REPO}"*}" || true
    echo "$URL"
}

function debug() {
    [ "${PLUGIN_DEBUG}" == "true" ] && echo "DEBUG: $*" 1>&2 || true
}

function craft_api_url_gitea() {
    # append to root url the api path
    API="$(extract_forge_url)/api/v1/repos/${CI_REPO}/issues?state=open"
    # replace '//' with '/'
    echo "$API" | sed 's/\/\//\//g'
}

function ignore_files_arg() {
  [ -f ".rgignore" ] && ARGS+=(--ignore-file .rgignore) || true
  [ -f ".ignore" ] && ARGS+=(--ignore-file .ignore) || true
  [ -f ".gitignore" ] && ARGS+=(--ignore-file .gitignore) || true
}

function check_wrong_todos() {
  # note https://github.com/rust-lang/regex/issues/127 did prevent us from inverted regex groups ...
  mapfile -t WRONG_TODOS < <(rg \
      --hidden \
      --ignore-case \
      --with-filename \
      --line-number \
      --only-matching \
      --regexp "${PLUGIN_PREFIX_REGEX}.*" \
      "${WORKDIR}" | rg \
        --invert-match \
        --regexp "${PLUGIN_PREFIX_REGEX}${PLUGIN_MAIN_REGEX}") || true

  for TODO in  "${WRONG_TODOS[@]}"; do
      TODO_ISSUE_FILE="$(echo "${TODO}" | cut -d ':' -f1-2)"
      echo "${TODO_ISSUE_FILE} -> found wrong TODO pattern"
  done

  [ -n "${WRONG_TODOS[0]}" ] && exit 255 || true
  debug no wrong TODO pattern found
}

function check_todos() {
    ISSUES=( "$@" )
    debug start check if TODOs have valid index numbers

    mapfile -t EXISTING_TODO_ISSUES < <(rg \
                              --hidden \
                              --ignore-case \
                              --with-filename \
                              --line-number \
                              --only-matching \
                              --regexp "${PLUGIN_PREFIX_REGEX}${PLUGIN_MAIN_REGEX}" \
                              --replace '$ISSUE_NUMBER' \
                              "${ARGS[@]}" \
                              "${WORKDIR}" \
                          )
    debug "EXISTING_TODO_ISSUES: $(echo "${EXISTING_TODO_ISSUES[@]}" | cut -d ':' -f3 | tr ' ' ', ')"

    FAIL="false"
    for TODO_ISSUE in "${EXISTING_TODO_ISSUES[@]}"; do
        local TODO_ISSUE_EXIST="false"
        TODO_ISSUE_FILE="$(echo "${TODO_ISSUE}" | cut -d ':' -f1-2)"
        TODO_ISSUE_NUMBER="$(echo "${TODO_ISSUE}" | cut -d ':' -f3)"

        for ISSUE in "${ISSUES[@]}"; do
            debug "'$ISSUE' == '$TODO_ISSUE_NUMBER'"
            [ "$ISSUE" == "$TODO_ISSUE_NUMBER" ] && TODO_ISSUE_EXIST="true" && break || true
        done
        [ "${TODO_ISSUE_EXIST}" == "false" ] && {
            echo "${TODO_ISSUE_FILE} -> found issue '${TODO_ISSUE_NUMBER}' but no corresponding open one in ${CI_FORGE_TYPE}"
            FAIL=true
        } || true
    done

    debug FAIL is "${FAIL}"

    [ "${FAIL}" == "true" ] && exit 255 || true
}

#########
# start #
#########
debug "workdir is '${WORKDIR}'"

# setup args
ignore_files_arg

check_wrong_todos

ISSUES_API_URL="$(craft_api_url_"${CI_FORGE_TYPE}")"
debug "make get request to '${ISSUES_API_URL}'"
CURL_OUTPUT=$(curl --silent --request "GET" "${ISSUES_API_URL}" --header "accept: application/json")
mapfile -t ISSUES < <(echo "$CURL_OUTPUT" | jq --raw-output ".[].number")
debug "open issues: $(echo "${ISSUES[@]}" | tr '\n' ', ')"
check_todos "${ISSUES[@]}"
